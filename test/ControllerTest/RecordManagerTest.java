package ControllerTest;

import Controller.RecordManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Shane
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RecordManagerTest {

    RecordManager m1;

    private final int RECORD_SIZE = 325;

//    String filePath = "H:\\SecondYear\\SecSem\\OOP\\Semester2\\CA5\\oop-ca5\\";
    String filePath = "//home//oshi//OOPCA//oop-ca5//test//ControllerTest//";
    RandomAccessFile raf;

    @Before
    public void setUp() throws Exception {
        m1 = new RecordManager(filePath);
        String fileName = "commonTest";
        m1.openDatabase(fileName);

    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of showCount method, of class m.
     *
     * @throws java.io.IOException
     */
    //****NEVER CHANGE THESE NAMES, THEY DETERMINE THE ORDER OR EXECUTION(ALPHABETICAL)
    @Test
    public void atestCreateDatabase() throws IOException {
        System.out.println("Create Database");
        File f = new File(filePath + "createTest.txt");
        boolean result = f.createNewFile();
        boolean expectedResult = true;
        assertEquals(expectedResult, result);
    }

    //****NEVER CHANGE THESE NAMES, THEY DETERMINE THE ORDER OR EXECUTION(ALPHABETICAL)
    @Test
    public void btestDeleteDatabase() throws IOException {
        System.out.println("Delete Database");
        File f = new File(filePath + "createTest.txt");
        boolean result = f.delete();
        boolean expectedResult = true;
        assertEquals(expectedResult, result);

    }

    //****NEVER CHANGE THESE NAMES, THEY DETERMINE THE ORDER OR EXECUTION(ALPHABETICAL)
    @Test
    public void testshowCount() throws FileNotFoundException, IOException {
        raf = new RandomAccessFile(filePath + "showCountTest.movie_db", "rw");
        System.out.println("showCount");
        int expResult = 1;
//        String result = m1.showCount();
        int count = (int) raf.length() / RECORD_SIZE;
        assertEquals(expResult, count);

    }

    //****NEVER CHANGE THESE NAMES, THEY DETERMINE THE ORDER OR EXECUTION(ALPHABETICAL)
    @Test
    public void ctestAddRecord() throws FileNotFoundException, IOException {
        raf = new RandomAccessFile(filePath + "addRecordTest.movie_db", "rw");
        Random random = new Random();
        System.out.println("Add Record");
        System.out.println(m1.showCount());
        int expectedResult = m1.showCount() + 1;
        String name = "test add";
        String desc = "test desc";
        double budget = random.nextDouble();
        double ticketprice = random.nextDouble();
        boolean is3d = random.nextBoolean();
        int runtime = random.nextInt();
        m1.addRecord(raf, name, desc, budget, ticketprice, is3d, runtime);
        assertEquals(expectedResult, m1.showCount());
        System.out.println(m1.showCount());

    }

    @Test
    public void edeleteRecord() throws FileNotFoundException, IOException {
        raf = new RandomAccessFile(filePath + "addRecordTest.movie_db", "rw");
        System.out.println("Delete record");
        boolean confirmation = true;
        int index = 0;
        System.out.println(m1.showCount());
        int expectedResult = (int) (m1.showCount() - 1);
        m1.deleteRecord(raf, confirmation, index);
        assertEquals(expectedResult, m1.showCount());

    }
    
    @Test 
    public void fdeleteAll() throws FileNotFoundException, IOException{
    raf = new RandomAccessFile(filePath + "addRecordTest.movie_db", "rw");
        System.out.println("Delete record All");
        raf.setLength(0);
        assertEquals(0, raf.length());
    }
}
