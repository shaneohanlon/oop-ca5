/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author oshi
 */
public class Movie {

    private String name;
    private String desc;
    private double budget;
    private double ticketPrice;
    private int runtime;
    private boolean is3D;

    public Movie() {
    }

    public Movie(String name, String desc, Double budget, Double ticketPrice, int runtime, boolean is3D) {
        this.name = name;
        this.desc = desc;
        this.budget = budget;
        this.ticketPrice = ticketPrice;
        this.runtime = runtime;
        this.is3D = is3D;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public double getBudget() {
        return budget;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public int getRuntime() {
        return runtime;
    }

    public boolean isIs3D() {
        return is3D;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public void setIs3D(boolean is3D) {
        this.is3D = is3D;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.desc);
        hash = 47 * hash + Objects.hashCode(this.budget);
        hash = 47 * hash + Objects.hashCode(this.ticketPrice);
        hash = 47 * hash + this.runtime;
        hash = 47 * hash + (this.is3D ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (this.runtime != other.runtime) {
            return false;
        }
        if (this.is3D != other.is3D) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.desc, other.desc)) {
            return false;
        }
        if (!Objects.equals(this.budget, other.budget)) {
            return false;
        }
        if (!Objects.equals(this.ticketPrice, other.ticketPrice)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Movies{" + "name=" + name + ", desc=" + desc + ", budget=" + budget + ", ticketPrice=" + ticketPrice + ", runtime=" + runtime + ", is3D=" + is3D + '}';
    }

    public void print() {
        System.out.println(this.name + "\t\t" + this.desc + "\t\t" + this.budget + "\t\t" + this.ticketPrice + "\t\t" + this.runtime + "\t\t" + this.is3D);
    }

}
