package Controller;

import Model.Movie;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class RecordManager {

    private final int RECORD_SIZE = 325;
    private final String filePath;
    private ArrayList<Movie> movieList;

    public RecordManager(String filePath) {
        this.filePath = filePath;
        this.movieList = new ArrayList<Movie>();
    }

    //create a new file with the given name
    public void createDatabase() throws IOException {
        String fileName = Utility.ScannerUtility.getString("Enter Database name :");
        File temp = new File(this.filePath + fileName + ".movie_db");
        temp.createNewFile();

    }

    //opens an existing database file and reads it to a list, creates a new file if it doesnt exist
    public RandomAccessFile openDatabase(String fileName) throws FileNotFoundException, IOException {

        RandomAccessFile raf = new RandomAccessFile(this.filePath + fileName + ".movie_db", "rw");
        readRecord(raf);
        return raf;
    }

    //deletes the file from the system
    public void deleteDatabase(boolean confirmation, String fileName) {
        if (confirmation) {
            File file = new File(this.filePath + fileName + ".movie_db");
            if (file.delete()) {
                System.out.println(file.getName() + " file deleted!");
            } else {
                System.out.println("Can't delete file.");
            }
        } else {
            System.out.println("Nothing deleted");
        }
    }

    //appends a new record at the end of the database file, and adds it to the movie list
    public void addRecord(RandomAccessFile raf, String name, String desc, double budget, double ticketPrice, boolean is3d, int runtime) throws IOException {
        name = Utility.StringUtility.pad(name, 45, "*");
        desc = Utility.StringUtility.pad(desc, 255, "*");
        Movie movie = new Movie(Utility.StringUtility.unpad(name, "*"), Utility.StringUtility.unpad(desc, "*"), budget, ticketPrice, runtime, is3d);
        if (movieList.contains(movie)) {
            System.out.println("Duplicate Entry, not added");
        } else {

            raf.seek(raf.length());
            raf.writeUTF(name);
            raf.writeUTF(desc);
            raf.writeDouble(budget);
            raf.writeDouble(ticketPrice);
            raf.writeInt(runtime);
            raf.writeBoolean(is3d);
            movieList.add(movie);
            System.out.println("New record added");
        }

    }

    //read the record in from the file and adds it to the list for easy access
    public void readRecord(RandomAccessFile raf) throws IOException {
        raf.seek(0);
        long numberOfRecords = raf.length() / RECORD_SIZE;
        for (long i = 0; i < numberOfRecords; i++) {

            raf.seek(i * RECORD_SIZE);
            movieList.add(new Movie(Utility.StringUtility.unpad(raf.readUTF(), "*"), Utility.StringUtility.unpad(raf.readUTF(), "*"), raf.readDouble(), raf.readDouble(), raf.readInt(), raf.readBoolean()));
        }
    }

    //deletes the record from the list and deletes the database file and makes a new file without the deleted record
    public void deleteRecord(RandomAccessFile raf, boolean confirmation, int index) throws IOException {
        if (confirmation) {
            if (index < movieList.size()) {
                movieList.remove((int) index);
                raf.setLength(0);
                for (Movie m : movieList) {
                    raf.writeUTF(Utility.StringUtility.pad(m.getName(), 45, "*"));
                    raf.writeUTF(Utility.StringUtility.pad(m.getName(), 255, "*"));
                    raf.writeDouble(m.getBudget());
                    raf.writeDouble(m.getTicketPrice());
                    raf.writeInt(m.getRuntime());
                    raf.writeBoolean(m.isIs3D());
                }
                System.out.println("Record at " + index + " deleted!");
            } else {
                System.out.println("Invalid index!");
            }
        } else {
            System.out.println("Nothing Deleted");
        }
    }

    //sets the size of the databse file to 0, affectively deleting everything in there, also deletes it from the list
    public void deleteAll(RandomAccessFile raf, boolean confirmation) throws IOException {

        if (confirmation) {
            raf.setLength(0);
            movieList.clear();

            System.out.println("All Records Deleted");
        } else {
            System.out.println("Nothing Deleted");
        }
    }

    //prints all the records in the database
    public void displayAll() {
        System.out.println("Name\t\tDesc\t\tBudget\t\tTicketPrice\t\tRuntime\t\tis3d");
        if (movieList.isEmpty()) {
            System.out.println("No Records found!");
        } else {
            for (Movie m : movieList) {

                m.print();
            }
        }
    }

    //returns the number of records in the database
    public int showCount() {

        return movieList.size();
    }

    //edit a records detail, seeks to the file pointer location of a field and overwrites it
    public void edit(RandomAccessFile raf, int index, int choice) throws IOException {

        int seekAmount = index * RECORD_SIZE;
        System.out.println(seekAmount);
        if (seekAmount < raf.length()) {
            raf.seek(seekAmount);

            switch (choice) {
                case 1:
                    String name = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Enter a new name(45 characters): "), 45, "*");
                    while (name.length() > 45) {
                        name = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Please enter a name under 45 characters "), 45, "*");
                    }
                    raf.writeUTF(name);
                    break;
                case 2:
                    String desc = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Enter a new Desc(255 characters): "), 255, "*");
                    while (desc.length() > 255) {
                        desc = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Please enter a name under 255 characters "), 255, "*");
                    }
                    raf.seek(47);
                    raf.writeUTF(desc);
                    break;
                case 3:
                    double budget = Utility.ScannerUtility.getDouble("Enter a new Budget : ");
                    raf.seek(304);
                    raf.writeDouble(budget);
                    break;
                case 4:
                    double price = Utility.ScannerUtility.getDouble("Enter a new Ticket Price : ");
                    raf.seek(312);
                    raf.writeDouble(price);
                    break;
                case 5:
                    int runtime = Utility.ScannerUtility.getInt("Enter a new runtime : ");
                    raf.seek(320);
                    raf.writeInt(runtime);
                    break;
                case 6:
                    boolean is3d = Utility.ScannerUtility.getBoolean("is3D? : ");
                    raf.seek(324);
                    raf.writeBoolean(is3d);
                    break;
                default:
                    System.out.println("Invalid Choice");
                    break;
            }
            movieList.clear();
            readRecord(raf);
        } else {
            System.out.println("Invalid index");
        }
    }

    //searches the database for a record with the given name
    public void searchByName(String searchCriteria) {
        System.out.println("\t\tSearching By Name");
        String output = "";

        for (Movie m : movieList) {
            if (m.getName().equals(searchCriteria)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that Name");
        } else {
            System.out.println(output);
        }
    }

    //searches the database for a record with the given description
    public void searchByDesc(String searchCriteria) {
        System.out.println("\t\tSearching By Desc");
        String output = " ";
        for (Movie m : movieList) {
            if (m.getDesc().equals(searchCriteria)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that Desc");
        } else {
            System.out.println(output);
        }
    }

    //searches the database for a record with the given budget
    public void searchBybudget(double searchCriteria) {
        System.out.println("\t\tSearching By Budget");
        String output = "";
        for (Movie m : movieList) {
            if (m.getBudget() == (searchCriteria)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that Budget");
        } else {
            System.out.println(output);
        }
    }

    //searches the database for a record with the given ticket price
    public void searchByTicketPrice(double searchCriteriaDouble) {

        System.out.println("\t\tSearching By Ticket Price");
        String output = "";
        for (Movie m : movieList) {
            if (m.getTicketPrice() == (searchCriteriaDouble)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that Tiket Price");
        } else {
            System.out.println(output);
        }
    }

    //searches the database for a record with the given runtime
    public void searchByRunTime(int searchCriteriaInt) {

        System.out.println("\t\tSearching By Runtime");
        String output = "";
        for (Movie m : movieList) {
            if (m.getBudget() == (searchCriteriaInt)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that Runtime");
        } else {
            System.out.println(output);
        }
    }

    //searches the database for a record with the given 3d value
    public void searchBy3D(boolean searchCriteriaBool) {

        System.out.println("\t\tSearching By is3D");
        String output = "";
        for (Movie m : movieList) {
            if (m.isIs3D() == (searchCriteriaBool)) {
                output += m.getName() + "\t" + m.getDesc() + "\t" + m.getBudget() + "\t" + m.getTicketPrice() + "\t" + m.getRuntime() + "\t" + m.isIs3D() + "\n";
            }
        }
        if (output.equals("")) {
            System.out.println("No Movie found by that is3D");
        } else {
            System.out.println(output);
        }
    }

}
